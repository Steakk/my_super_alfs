import os

from src import tools

def print_logo():
    print(tools.Color.B)
    print("     ___       __       _______     _______.")
    print("    /   \     |  |     |   ____|   /       |")
    print("   /  ^  \    |  |     |  |__     |   (----`")
    print("  /  /_\  \   |  |     |   __|     \   \    ")
    print(" /  _____  \  |  `----.|  |    .----)   |   ")
    print("/__/     \__\ |_______||__|    |_______/    ")
    print("                                            ")
    print("   Automated Linux From Scratch             ")
    print("                                            ")
    print(tools.Color.E)

def main():
    print("Before continuing this, check that you have the good versions for")
    print("a LFS. Moreover, this is a script made by students with little")
    print("experience in doing LFS, so there might be a lot of problems with")
    print("your personnal architecture.\n")

    ipt = input("Continue? [Y/n] ")

    if not tools.check_answer(ipt):
        return 0

    print('You chose to continue. Great.')
    print('This script is the main script, it doesn\'t do anything to your')
    print('computer, but it will run other script that will do. So if one scipt')
    print('stops because of an error, you just have to continue from the')
    print('corresponding script.\n')

    print('Launching build_lfs script...\n')

    otp = os.system('python3 src/build_lfs.py')

    if otp > 0:
        print('An error might have occured during build_lfs script.')
        return 2

    print('build_lfs script finished.')
    return 0

if __name__ == "__main__":
    tools.console_clear()
    print_logo()

    exit(main())
