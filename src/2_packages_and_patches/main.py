import os
import stat
import shutil

# Setup the required directories in the lfs root
def setup_dir(lfs_dir):
    print('Creating required directory...')

    os.makedirs(lfs_dir + '/sources', exist_ok=True)
    os.system('chmod -v a+wt ' + lfs_dir + '/sources')

    print('Created.')

# Download the required packages in the $LFS/sources directory
def download_packages(sources_dir):
    print('Downloading packages...')
    os.system('wget --input-file=wget-list --continue --directory-prefix='
            + sources_dir)
    print('Downloaded.')

# Check the downloaded file with md5sum
def sum_check(sources_dir):
    ans = input('Do you want to check the downloaded packages ? [y/n]').lower()
    if ans == 'y' or ans == 'yes':
        shutil.copyfile('md5sums', sources_dir + '/md5sums')
        os.system('pushd ' + sources_dir + ' && md5sum -c md5sums && popd')

def main(data):
    sources_dir = lfs_path + '/sources'
    setup_dir(lfs_path)
    download_packages(sources_dir)
    sum_check(sources_dir)

if __name__ == '__main__':
    setup_dir('test')
    download_packages('test/sources')
    sum_check('test/sources')
