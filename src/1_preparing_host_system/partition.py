import sys
import os
import re
import subprocess

sys.path.append('..')
import tools

def partition_format(fdisk_output):
    res = re.findall('/dev/[^loop.*].*', fdisk_output)
    
    for e in res:
        print(e)

    return fdisk_output

def partition(data):
    print('Welcome to the partition script, we will now do all the partition')
    print('needed for your alfs to be installed correctly.')

    cmd = "echo " + data.passwd + " | sudo fdisk -l"
    res = tools.run(cmd)

    partition_format(res.stdout.decode('UTF-8'))

    input("Give your disk (min 10GB unallocated) (example: /dev/sda): ")

    configs = [f for f in listdir("src/1_preparing_host_system/partition_models")]

    for i, e in enumerate(configs):
        print(i + "/ " + configs)

    input("Choose a partitionning configuration: ")

if __name__ == '__main__':
    partition('plop')
