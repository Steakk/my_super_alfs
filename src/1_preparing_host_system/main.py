import os
import subprocess
import re

import importlib

def main(data):
    importlib.import_module("1_preparing_host_system.check_dependencies", package=None).check_dependencies(data)
    importlib.import_module("1_preparing_host_system.partition", package=None).partition(data)

if __name__ == "__main__":
    check_dep()
    check_links()
