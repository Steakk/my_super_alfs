from os import system, path, listdir, chdir
from importlib import import_module
from getpass import getpass

from tools import *

def get_steps():
    folders = listdir(FOLDER)
    folders.sort()

    # Remove all folders that are not part of the lfs:
    remove = True

    while remove:
        remove = False
        for name in folders:
            if not name[0].isdigit():
                folders.remove(name)
                remove = True
                break

    # print the steps of building:
    for step in folders:
        print(" ", step)
    print("")

    return folders

def get_data():
    print('For most of the operations that will follow, we will need to have')
    print('superuser rights. So please enter your password.')
    pwd = getpass()

    lfs_path = input("Give the path where the lfs will be mounted: (/mnt/lfs by default) ")
    data = Data(pwd) if lfs_path == '' else Data(pwd, lfs_path)

    console_clear()
    return data

def build(folders, data):
    for folder in folders:
        print(Color.H + folder + Color.E)
        main_path = path.join(FOLDER, folder, 'main.py')
        print(main_path)

        if not path.exists(main_path):
            print(F + folder + ": main does not exist." + E)
            exit(1)

        imported_module = import_module(folder + "." + "main")

        chdir(path.join(FOLDER, folder))
        imported_module.main(data)
        chdir("../../")

        if not check_answer(input("Continue ? [Y/n] ")):
            exit(0)
        
        console_clear()

if __name__ == '__main__':
    print("Build lfs steps:")
    steps = get_steps()
    data = get_data()
    build(steps, data)
