import subprocess

from os import system, name

# Paths
FOLDER = './src'
LFS = '/mnt/lfs'

# Color
class Color:
    H = '\033[1m'
    B = '\033[94m'
    T = '\033[92m'
    F = '\033[91m'
    W = '\033[93m'
    E = '\033[0m'

class Data:
    def __init__(self, passwd, lfs_path = LFS):
        self.passwd = passwd
        self.lfs_path = lfs_path

def check_answer(ans):
    ans = ans.lower()
    return ans == 'y' or ans == 'yes'

def run(cmd):
    result = subprocess.run(cmd, check=True, shell=True,
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return result

def console_clear():
    if name == 'nt':
        _ = system('cls')
    else:
        _ = system('clear')
