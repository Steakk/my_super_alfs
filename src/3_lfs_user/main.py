import os

def add_user():
    os.system('groupadd lfs')
    os.system('useradd -s /bin/bash -g lfs -m -k /dev/null lfs')

def setup_dirs(lfs_dir):
    print('Creating required directories...')

    dirs = ['/bin', '/etc', '/lib', '/sbin', '/usr', '/var',
            '/tools']
    for dir in dirs:
        os.makedirs(lfs_dir + dir, exist_ok=True)
        os.system('chmod -v ' + lfs_dir + dirs[0])

    if os.uname().machine == 'x86_64':
        os.makedirs(lfs_dir + '/lib64', exist_ok=True)

    print('Created.')

def main(data):
    add_user()
    setup_dirs(lfs_path)

if __name__ == '__main__':
    add_user()
    setup_dirs('test')
