from os import listdir
from importlib import import_module
import sys
sys.path.append('../')

from tools import *

def main(pwd):
    print("Build temporary tools:")
    files = listdir(FOLDER)

    if '__pycache__' in files:
        files.remove('__pycache__')

    if __file__ in files:
        files.remove(__file__)

    for build_file in files:
        # remove .py extension from file's name:
        build_file = build_file[:-3]

        print(H + build_file + ":" + E)
        imported_module = import_module(FOLDER + '.' + build_file)

        build_functions = [e for e in dir(imported_module) if e[0] != '_']

        for build_function in build_functions:
            result = getattr(imported_module, build_function)() # Parameters
            print(H + build_file + '::' + build_function + E, T + "OK" + E if result else F + "FAIL" + E)
